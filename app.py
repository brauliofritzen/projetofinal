import flask

app = flask.Flask(__name__)

@app.route("/")
def ola():
  
    return flask.render_template("index.html")

@app.route("/ubber", methods=['POST'])
def ubber():
    dados = flask.request.get_json()
    a = dados['message']
      
    return {"message": a.upper()}
    
app.run(host="0.0.0.0", port=80)


